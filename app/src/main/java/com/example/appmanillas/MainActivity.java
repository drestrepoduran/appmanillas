package com.example.appmanillas;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText input_quantity;
    private TextView label_result, label_unit_value;
    private Spinner spinnerMaterial, spinnerDije, spinnerType, spinnerCurrency;
    private String materials[], dijes[], types[], currencies[];
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input_quantity = findViewById(R.id.inputQuantity);
        label_result = findViewById(R.id.labelResult);
        label_unit_value = findViewById(R.id.labelUnitValue);
        spinnerMaterial = findViewById(R.id.spinnerMaterial);
        spinnerDije = findViewById(R.id.spinnerDije);
        spinnerType = findViewById(R.id.spinnerType);
        spinnerCurrency = findViewById(R.id.spinnerCurrency);

        //materials
        materials = getResources().getStringArray(R.array.materials);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,materials);
        spinnerMaterial.setAdapter(adapter);

        //dijes
        dijes = getResources().getStringArray(R.array.dije);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,dijes);
        spinnerDije.setAdapter(adapter);


        //types
        types = getResources().getStringArray(R.array.types);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,types);
        spinnerType.setAdapter(adapter);

        //currencies
        currencies = getResources().getStringArray(R.array.currencies);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,currencies);
        spinnerCurrency.setAdapter(adapter);

    }

    public void calculate(View v){

        if(validate()){
            Double unit_value = unitValue(spinnerMaterial.getSelectedItemPosition(),
                    spinnerDije.getSelectedItemPosition(),spinnerType.getSelectedItemPosition(),
                    spinnerCurrency.getSelectedItemPosition());

            Double total = unit_value * Double.parseDouble(input_quantity.getText().toString());
            label_unit_value.setText(unit_value.toString());

            label_result.setText(total.toString());
        }


    }

    public Double unitValue(int material, int dije, int type, int currency){
        //leather
        Double unit_value = 0.0;
        //cuero
        if(material == 0){
            //martillo
            if(dije == 0){
                switch (type) {
                    //oro
                    case 0: case 1:
                        unit_value = 100.0;
                        break;
                    //plata
                    case 2:
                        unit_value = 80.0;
                        break;
                    //niquel
                    case 3:
                        unit_value = 70.0;
                        break;
                }
             //ancla
            }else{
                switch (type) {
                    //oro
                    case 0: case 1:
                        unit_value = 120.0;
                        break;
                    //plata
                    case 2:
                        unit_value = 100.0;
                        break;
                    //niquel
                    case 3:
                        unit_value = 90.0;
                        break;
                }
            }
        //cuerda
        }else{
            //martillo
            if(dije == 0){
                switch (type) {
                    //oro
                    case 0: case 1:
                        unit_value = 90.0;
                        break;
                    //plata
                    case 2:
                        unit_value = 70.0;
                        break;
                    //niquel
                    case 3:
                        unit_value = 50.0;
                        break;
                }
            //ancla
            }else{
                switch (type) {
                    //oro
                    case 0: case 1:
                        unit_value = 110.0;
                        break;
                    //plata
                    case 2:
                        unit_value = 90.0;
                        break;
                    //niquel
                    case 3:
                        unit_value = 80.0;
                        break;
                }
            }
        }

        //pesos
        if(currency == 0){
            unit_value = unit_value * 3200.0;
        }

        return unit_value;
    }

    public boolean validate(){
        if(input_quantity.getText().toString().isEmpty()){
            input_quantity.setError(getString(R.string.labelError));
            input_quantity.requestFocus();
            return false;
        }
        return true;
    }
}